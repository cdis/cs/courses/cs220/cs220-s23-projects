# Project 5 (P5) grading rubric

## Code reviews

- A TA  or  grader will be reviewing your code after the deadline.
- They will make (4) based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.
- Incorrect function logic loses points in manual code review

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-3)
- Used concepts or modules (ex: lists, dictionaries, or pandas) not covered in class yet (built-in functions that you have been introduced to can be used) (-3)
- Hardcoded answers (full points for each hardcoded question)

### Question specific guidelines:

- Q1 (2)
	- Required function is not used (-1)

- Q2 (3)
	- Required function is not used (-1)

- Q3 (4)
	- Index of the last hurricane is hardcoded (-3)
	- Required function is not used (-1)

- Q4 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used (-1)

- Q5 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used (-1)

- Function 1 (4)
  - Function logic is incorrect (-3)
	- Function is defined more than once (-1)

- Q6 (4)
	- Did not exit loop and instead iterated further after finding the hurricane named `Igor` (-2)
	- Required functions are not used to answer (-1)

- Q7 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q8 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q9 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q10 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Function 2 (2)
  - Function logic is incorrect (-1)
	- Function is defined more than once (-1)

- Function 3 (2)
  - Function logic is incorrect (-1)
	- Function is defined more than once (-1)

- Function 4 (2)
  - Function logic is incorrect (-1)
	- Function is defined more than once (-1)

- Q11 (5)
	- Incorrect logic is used to answer (-2)
	- Used indices of the hurricanes to determine the earliest hurricane (-1)
	- Variable to store the index or name of the earliest hurricane is not initialized as `None` (-1)
	- Required functions are not used to answer (-1)

- Q12 (5)
	- Incorrect logic is used to answer (-2)
	- Used indices of the hurricanes to determine the earliest hurricane (-1)
	- Variable to store the index or name of the earliest hurricane is not initialized as `None` (-1)
	- Required functions are not used to answer (-1)

- Function 5 (4)
  - Function logic is incorrect (-1)
	- Variable to store the index of the deadliest hurricane is not initialized as `None` (-1)
	- Required functions are not used to answer (-1)
	- Function is defined more than once (-1)

- Q13 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-2)

- Q14 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-2)

- Q15 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Function 6 (4)
  - Function logic is incorrect (-3)
	- Function is defined more than once (-1)

- Q16 (4)
	- Required functions are not used to answer (-2)

- Q17 (5)
	- Incorrect logic is used to answer (-2)
	- Didn't loop through the years in the last decade and hardcoded all ten years (-2)
	- Required functions are not used to answer (-1)

- Q18 (5)
	- Incorrect logic is used to answer (-2)
	- `year_with_most_hurricanes` is not initialized as some year in the twentieth century, or as `None` (-2)
	- Required functions are not used to answer (-1)

- Q19 (4)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-1)

- Q20 (5)
	- Incorrect logic is used to answer (-2)
	- Required functions are not used to answer (-2)
