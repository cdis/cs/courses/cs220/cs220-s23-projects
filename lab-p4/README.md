# Lab-P4: Conditional Statements and Pokémon API

In P4, you will be playing with some Pokémon and you will simulate simple Pokémon battles using conditional statements. In Lab-P4, you will learn to use `project.py`, which you will need to complete P4. You will also be introduced to some simple conditional statements and 'helper functions' which will be useful for P4.

### Corrections/Clarifications

None yet

**Find any issues?** Report to us:

- Hakan Dingenc <dingenc@wisc.edu>
- Jodi Lawson <jlawson6@wisc.edu>

------------------------------
## Learning Objectives

In this lab, you will practice...
* Learning and using an 'API' (Application Programming Interface)
* Building 'helper' functions that can be used to create more advanced functions
* Writing conditions using if/elif/else statements
* Writing advanced conditions using nested if/else statements
* Writing advanced conditions using logical operators (or/and)

------------------------------
## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start working on Lab-P4 with one person, then do the project with a different partner.  Now may be a good time to review [our course policies](https://cs220.cs.wisc.edu/s23/syllabus.html).

------------------------------

## Project partner

We strongly recommend students find a project partner. Pair programming is a great way to learn from a fellow student. Project difficulty increases exponentially in this course. Finding a project partner early on during the semester is a good idea.

If you are still looking for a project partner, take a moment now to ask around the room if anyone would like to partner with you on this project. Then you can work with them on this lab as well as the project.

------------------------------
## Segment 1: Setup

Create a `lab-p4` directory and download the following files into the `lab-p4` directory:

* `project.py`
* `pokemon_stats.csv`
* `type_effectiveness_stats.csv`
* `practice.ipynb`
* `practice_test.py`

**Note:** If you accidentally downloaded the file as a `.txt` (or `.cvs` or `.csv.txt`) instead of `.csv` (say `pokemon_stats.txt`), you can execute `mv pokemon_stats.txt pokemon_stats.csv` on a Terminal/PowerShell window. Recall that the `mv` (move) command lets you rename a source file (first argument in example: `pokemon_stats.txt`) to the destination file (second argument in example: `pokemon_stats.csv`).

Once you have downloaded the files, open a Terminal/PowerShell window and navigate to your `lab-p4` directory.  Run `ls` to make sure the above files are available.

------------------------------
## Segment 2: Learning the API

You will be finishing the rest of your lab on `practice.ipynb`. Run the command `jupyter notebook` from your Terminal/PowerShell window. Remember not to close this Terminal/PowerShell window while Jupyter is running, and open a new Terminal/PowerShell window if necessary.

**Note:** For P4, you will be working on `p4.ipynb` which is very similar to `practice.ipynb`.

**Note:** Unlike `p4.ipynb`, you do **not** have to submit `practice.ipynb`. This notebook is solely for your practice.

------------------------------

You can now get started with [P4](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projects/-/tree/main/p4). **You can copy/paste and use any helper function that you have created here in P4.** Good luck and have fun!
