# Project 6 (P6): Investigating Airbnb Data

## Corrections and clarifications:

* **3/1/2023 - 12:45 PM**: A bug in `p6_test.py` that caused the notebook to not export has been fixed. Please redownload `p6_test.py` if you are facing this issue. You will **not** have to do anything to your notebook to fix this.
* **3/2/2023 - 12:45 PM**: Typos in the rubric fixed; function rubric deductions broken down further.
* **3/3/2023 - 8:45 PM**: Points allotted to Q19 changed from 3 to 4, so the total adds up to 100.

**Find any issues?** Report to us:

- Jane Zhang <zhang2752@wisc.edu>
- Kartik Munjal <munjal3@wisc.edu>

## Note on Academic Misconduct:
You are **allowed** to work with a partner on your projects. While it is not required that you work with a partner, it is **recommended** that you find a project partner as soon as possible as the projects will get progressively harder. Be careful **not** to work with more than one partner. If you worked with a partner on Lab-P6, you are **not** allowed to finish your project with a different partner. You may either continue to work with the same partner, or work on P6 alone. Now may be a good time to review our [course policies](https://cs220.cs.wisc.edu/s23/syllabus.html).

## Instructions:

This project will focus on **accessing data in csv file** and **string methods**. To start, download [`p6.ipynb`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projectdesign/-/raw/main/p6/p6.ipynb), [`p6_test.py`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projectdesign/-/raw/main/p6/p6_test.py) and [`airbnb.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projectdesign/-/raw/main/p6/airbnb.csv).

If it takes too long to load the file `airbnb.csv` on GitLab, you can directly download the file from this link: [https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projects/-/raw/main/p6/airbnb.csv](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projects/-/raw/main/p6/airbnb.csv). You will need to **Right Click**, and click on the **Save as...** button to save the file through this method.

**Note:** Please go through [Lab-P6](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projects/-/tree/main/lab-p6) before you start the project. The lab contains some very important information that will be necessary for you to finish the project.

You will work on `p6.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**.

After you've downloaded the file to your `p6` directory, open a terminal window and use `cd` to navigate to that directory. To make sure you're in the correct directory in the terminal, type `pwd`. To make sure you've downloaded the notebook file, type `ls` to ensure that `p6.ipynb`, `p6_test.py`, and `airbnb.csv` are listed. Then run the command `jupyter notebook` to start Jupyter, and get started on the project!

**IMPORTANT**: You should **NOT** terminate/close the session where you run the above command. If you need to use any other Terminal/PowerShell commands, open a new window instead. Keep constantly saving your notebook file, by either clicking the "Save and Checkpoint" button (floppy disk) or using the appropriate keyboard shortcut.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projects/-/tree/main/p6/rubric.md), to ensure that you don't lose points during code review.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P6 assignment.
- If you completed the project with a **partner**, make sure to **add their name** by clicking "Add Group Member"
in Gradescope when uploading the P6 zip file.

   <img src="images/add_group_member.png" width="400">

   **Warning:** You will have to add your partner on Gradescope even if you have filled out this information in your `p6.ipynb` notebook.

- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available in a few minutes after your submission. You should be able to see both PASS / FAIL results for the 20 test cases and your total score, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="400">

    Note that you can only see your score as `-/100.0` since it has not yet been reviewed by a TA. However, you should confirm that your tests have all passed the autograder.

- In case you believe that you have not received the correct score for your project after it has been graded, make a **regrade request**, and wait for a grader to review your request.
