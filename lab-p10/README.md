# Lab-P10: Files and Namedtuples

In this lab, you'll get practice with files and namedtuples, in preparation for P10.

-----------------------------
## Corrections/Clarifications


**Find any issues?** Please report to us:

- Jane Zhang <zhang2752@wisc.edu>
- Abinayaa S Kanimozhi Chandrasekar <kanimozhicha@wisc.edu>

------------------------------
## Learning Objectives

In this lab, you will practice...
* Loading data in json files
* Loading data in csv files
* Using try/except to handle malformed data.

------------------------------

## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start 
working on Lab-P10 with one person, then do the project with a different partner. Now may be a 
good time to review [our course policies](https://cs220.cs.wisc.edu/s23/syllabus.html).

**Important:** P10 and P11 are two parts of the same data analysis.
You **cannot** switch project partners between these two projects.
If you partner up with someone for P10, you have to sustain that partnership until end of P11.

------------------------------

## Segment 1: Setup

Create a `lab-p10` directory and download the following files into the `lab-p10` directory.

* `small_data.zip`
* `practice.ipynb`
* `practice_test.py`

After downloading `small_data.zip`, make sure to extract it (using [Mac directions](http://osxdaily.com/2017/11/05/how-open-zip-file-mac/) or [Windows directions](https://support.microsoft.com/en-us/help/4028088/windows-zip-and-unzip-files)). After extracting, you should see a folder called `small_data`, which has the following files in it:

*` mapping_1.json`
* `mapping_2.json`
* `mapping_3.json`
* `stars_1.csv`
* `stars_2.csv`
* `stars_3.csv`
* `planets_1.csv`
* `planets_2.csv`
* `planets_3.csv`

You may delete `small_data.zip` after extracting these files from it.


## Segment 2: 
For the remaining segments, detailed instructions are provided in `practice.ipynb`. From the terminal, open a `jupyter notebook` session, open your `practice.ipynb`, and follow the instructions in `practice.ipynb`.

## Project 10

You can now get started with [P10](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s23-projects/-/tree/main/p10). **You may copy/paste any code created here in project P10**. Have fun!
